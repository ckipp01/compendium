#!/usr/bin/env node

let http = require("http")
let https = require("https")
let fs = require("fs")
let process = require("process")
let readline = require("readline")
let os = require("os")
let indental = require("./indental.js")

let HOME_DIRECTORY = os.homedir()
let STORAGE_DIRECTORY = HOME_DIRECTORY + "/.compendium"
let SITES_FILE = STORAGE_DIRECTORY + "/sites.js"
let WIKIS_FILE = STORAGE_DIRECTORY + "/wikis.json"
let wikis
let path = []
let view

if (!fs.existsSync(STORAGE_DIRECTORY)){
  fs.mkdirSync(STORAGE_DIRECTORY, {recursive: true})
}

function fetch(url){
  return new Promise(function(resolve, reject){
    try {
      https.get(url, function(response){
        let rawData = ""
        let hasEnded = false

        response.on("data", function(data){
          rawData += data
        })

        response.on("end", function(){
          hasEnded = true
          return resolve(rawData)
        })

        setTimeout(function(){
          if (!hasEnded) {
            console.log(url + " timed out!")
            return reject()
          }
        }, 5000)
      })
    } catch(e) {
      console.log(url + " could not be fetched!")
      return reject()
    }
  })
}

function getCurrent(){
  let obj = wikis

  path.forEach(function(key){
    obj = obj[key]
  })

  let view = Array.isArray(obj) ? "array" : "dict"
  return {obj, view}
}

let oldLog = console.log

console.log = function(text, count){
  count = isNaN(count) ? 2 : count
  let spaces = ""
  for (let i=0; i<count; i++) spaces += " "
  oldLog(spaces + text)
}

async function update(){
  console.log("Updating...")

  let data = await fetch("https://webring.xxiivv.com/scripts/sites.js")
  data += "\nmodule.exports = sites"
  fs.writeFileSync(SITES_FILE, data, "utf8")
  let sites = require(SITES_FILE)

  let promises = sites.filter(site => !!site.glossary).map(async function(site){
    console.log("Fetching " + site.author + "'s glossary...")
    let data = await fetch(site.glossary)
    let wiki = indental(data)
    if (!wikis) wikis = {}
    wikis[site.author.toUpperCase()] = wiki
  })

  await Promise.all(promises)
  fs.writeFileSync(WIKIS_FILE, JSON.stringify(wikis), "utf8")
  console.log("Updated!")
}

async function list(){
  console.log("=====")
  console.log(path.length > 0 ? path[path.length-1] : "COMPENDIUM")
  console.log("=====")
  console.log("")

  let current = getCurrent()

  if (current.view === "array"){
    current.obj.forEach(function(item){
      console.log(item)
    })
  } else if (current.view === "dict"){
    let stops = []
    let others = []

    Object.keys(current.obj).forEach(function(key){
      let child = current.obj[key]
      let type = typeof(child)

      if (type === "string" || type === "number") stops.push(key)
      else others.push(key)
    })

    others.forEach(function(key, i){
      console.log(i + ") " + key)
    })

    if (others.length > 0 && stops.length > 0) console.log("")

    stops.forEach(function(key){
      let child = current.obj[key]
      console.log(key + " : " + child)
    })
  } else {
    console.log("I'm not sure what do with this data:")
    console.log(current.obj)
  }
  
}

async function cd(key){
  if (key === "/") {
    path = []
    await list()
  } else if (key === "..") {
    path.splice(path.length-1, 1)
    await list()
  } else {
    try {
      key = JSON.parse(key)
    } catch(e) {
      console.log("Please use integer indices.")
      return
    }

    let currentObject = getCurrent().obj
    
    if (key < 0 || key > Object.keys(currentObject).length){
      console.log("Invalid index.")
    }

    key = Object.keys(currentObject)[key]

    let child = currentObject[key]
    let type = typeof(child)

    if (type === "string" || type === "number"){
      console.log("You can't go any further here. Use `cd ..` to move to the parent directory.")
      return
    }

    path.push(key)
    await list()
  }
}

let intro = `Welcome to the Compendium! Commands are:

    - update
    - list / ls
    - cd <index> / goto <index>
      - to move to a parent directory, use \`cd ..\`
      - to move back to the root, use \`cd /\`
    - quit / exit
      - or press ctrl+c
    - help

  Files are cached locally at ~/.compendium. See https://gitlab.com/jrc03c/compendium for more info.`

console.log("")
console.log(intro, 0)
console.log("")

let rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: "compendium > ",
})

rl.prompt()

rl.on("line", async function(line){
  let command = line.trim()
  let parts = command.split(" ")

  if (parts[0] === "help") console.log(intro, 0)
  if (parts[0] === "exit" || parts[0] === "quit") return rl.close()

  console.log("")
  if (parts[0] === "update") await update()

  if (!wikis){
    try {
      wikis = JSON.parse(fs.readFileSync(WIKIS_FILE, "utf8"))
    } catch(e) {
      console.log("The Compendium needs to be updated.")
      await update()
      console.log("")
    }
  }
  
  if (parts[0] === "list" || parts[0] === "ls") await list()
  if (parts[0] === "cd" || parts[0] === "goto") await cd(parts[1])

  console.log("")
  rl.prompt()
})

rl.on("close", function(){
  console.log("\n")
  process.exit(0)
})