Compendium is a browsable, offline-first collection of [the Merveilles webring](https://webring.xxiivv.com/#)'s glossaries.

Install it with:

```bash
$ npm install -g https://gitlab.com/jrc03c/compendium
```

And start it with:

```bash
$ compendium
```

This will launch a simple REPL, from which you can enter these commands:

`update` : updates the glossary cache

`list` or `ls` : lists the items at the current location in the Compendium

`cd <index>` or `goto <index>` : sets the current location to be inside of whichever object is selected by the index

`quit` or `exit` : exits the REPL

`help` : displays a short intro and these commands

Compendium caches its files at ~/.compendium. To uninstall Compendium, simply delete that folder and then do:

```bash
$ npm uninstall -g compendium
```